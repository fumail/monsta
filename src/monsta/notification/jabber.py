# -*- coding: UTF-8 -*-
from monsta.notification import Messenger
import logging
import time

try:
    import xmpp
    XMPP_AVAILABLE = True
except ImportError:
    XMPP_AVAILABLE = False

try:
    from dns import resolver
    DNSPYTHON_AVAILABLE = True
except ImportError:
    DNSPYTHON_AVAILABLE = False





class JabberMessenger(Messenger):
    """Notifications over JABBER/XMPP"""
    
    def __init__(self):
        Messenger.__init__(self)
        self.configvars['jid']=None
        self.configvars['password']=None
        
        self.helpstrings['jid']='jabber id (example: yourname@jabber.org)'
        self.helpstrings['password']='jabber account password'
        self.helpstrings['recipient']='alert recipient jid'
    
    
    # xmpppy is too dumb to do this itself...
    def _get_srv_rec(self, domain):
        server = port = None
        host = f'_xmpp-client._tcp.{domain}'
        dnsquery = resolver.query(host, 'SRV')
        for rec in dnsquery:
            port = rec.port
            server = rec.target.to_text()
        return server, port
    
    
    def _get_xmpp_conn(self):
        error = None
        jidaddr = self.configvars['jid']
        domain = jidaddr.rsplit('@',1)[-1]
        
        server, port = self._get_srv_rec(domain)
        if server is None:
            raise Exception('Could not get XMPP server for domain {domain}')
        jid=xmpp.protocol.JID(jidaddr)
        cl=xmpp.Client(jid.getDomain(),debug=[])
        con=cl.connect(server=(server, port)) # server and port MUST be specified in python3...
        if not con:
            error = "XMPP Connection failed"
        auth = cl.auth(jid.getNode(), self.configvars['password'], resource=jid.getResource())
        if not auth:
            error = "XMPP Auth failed"
        return cl, error
        
        
    def lint(self):
        if not XMPP_AVAILABLE:
            print("You are trying to use the jabber messenger, but the python xmpp lib (xmpppy) is not installed.")
            return False
        
        if not DNSPYTHON_AVAILABLE:
            print('You are trying to use the jabber messenger, but the dnspython dependency is not installed.')
            return False
            
        cl, error = self._get_xmpp_conn()
        if error is not None:
            print(error)
            return False
            
        #msgid=cl.send(xmpp.protocol.Message(self.recipient,'just a monsta lint'))
        #print(f'test jabber message sent with id {msgid}')
        
        return True 
    
     
    def send_message(self,message,subject=None):
        cl, error = self._get_xmpp_conn()
        if error is not None:
            raise Exception(error)
        
        msgid=cl.send(xmpp.protocol.Message(self.recipient,message))
        
        logging.debug("XMPP message sent. ID=%s"%msgid)
        
        time.sleep(1)
        cl.disconnect()
        
        