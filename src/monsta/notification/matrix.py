# -*- coding: UTF-8 -*-
from monsta.notification import Messenger
import logging
import asyncio

try:
    from nio import AsyncClient, MatrixRoom, RoomMessageText
    NIO_AVAILABLE = True
except ImportError:
    NIO_AVAILABLE = False



class MatrixMessenger(Messenger):
    """Notifications over JABBER/XMPP"""
    
    def __init__(self):
        Messenger.__init__(self)
        self.configvars['homeserver'] = None
        self.configvars['username'] = None
        self.configvars['password'] = None
        self.configvars['devicename'] = None
        
        self.helpstrings['homeserver'] = 'matrix homeserver to connect to'
        self.helpstrings['username'] = 'matrix user id (example: @yourname:matrix.io)'
        self.helpstrings['password'] = 'matrix user password'
        self.helpstrings['devicename'] = 'matrix device name'
        self.helpstrings['recipient'] = 'alert recipient matrix room name'
    
    
    def _get_device_name(self):
        devicename = self.configvars['devicename']
        if devicename is None:
            return devicename
        
        length = len(devicename)
        if length > 10:
            devicename = devicename[:10]
        elif length < 10:
            devicename += devicename[-1]*(10-length)
        return devicename
    
    
    async def _get_matrix_con(self):
        # https://matrix-nio.readthedocs.io/en/latest/examples.html
        client = AsyncClient(self.configvars['homeserver'], self.configvars['username'])
        devicename = self._get_device_name()
        if devicename is not None:
            client.device_id = devicename
        # setting device_name in login seems to have no effect
        resp = await client.login(self.configvars['password'], device_name=devicename)
        
        return client, resp
    
    
    async def _send_message(self, message):
        client, resp = await self._get_matrix_con()
        logging.debug(resp)
        
        joined = await client.join(self.configvars['recipient'])
        if joined.transport_response.status != 200:
            logging.error(f'failed to join room {self.configvars["recipient"]}, got response status {joined.transport_response.status}')
            logging.debug(joined)
        
        msgid = await client.room_send(
            # Watch out! If you join an old room you'll see lots of old messages
            room_id=self.configvars['recipient'],
            message_type="m.room.message",
            content={
                "msgtype": "m.text",
                "body": message
            }
        )
        
        if msgid.transport_response.status != 200:
            logging.error(f'failed to send matrix message to {self.configvars["recipient"]}, got response status {msgid.transport_response.status}')
            logging.debug(msgid)
        await client.close()
    
    
    async def _lint_con(self):
        client, resp = await self._get_matrix_con()
        print(resp)
        await client.close()
    
    
    def lint(self):
        if not NIO_AVAILABLE:
            print("You are trying to use the matrix messenger, but the python matrix lib (matrix-nio) is not installed.")
            return False
        
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self._lint_con())
        loop.close()
        #self.send_message('monsta test')
        
        return True
    
    
    def send_message(self, message, subject=None):
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self._send_message(message))
        loop.close()